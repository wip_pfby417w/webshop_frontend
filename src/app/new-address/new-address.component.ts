/**
 * @author Jonas Mertens
 * Komponente new Address
 * Neue Adresse zum Benutzer anlegen
 */
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {Address} from '../_models/address';

@Component({
  selector: 'app-new-address',
  templateUrl: './new-address.component.html',
  styleUrls: ['./new-address.component.css']
})
export class NewAddressComponent implements OnInit {

  public newAddressForm: FormGroup;
  public isSubmitted: boolean;
  public data: Address;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<NewAddressComponent>) {
    this.data = new Address();
  }

  get newAddressFormControls() {
    return this.newAddressForm.controls;
  }

  /**
   * Initialisierung des Formulars
   */
  ngOnInit(): void {
    this.newAddressForm = this.formBuilder.group(
      {
        forename: [''],
        surname: [''],
        street: [''],
        housenumber: [''],
        postcode: [''],
        city: ['']
      }
    );
  }

  ngOnChange(): void {
    this.ngOnInit();
  }

  /**
   * neue Adresse speichern
   */
  onSubmit() {
    this.isSubmitted = true;
    const returnAddress: Address = new Address();
    returnAddress.forename = this.newAddressForm.value.forename;
    returnAddress.surname = this.newAddressForm.value.surname;
    returnAddress.street = this.newAddressForm.value.street;
    returnAddress.housenumber = this.newAddressForm.value.housenumber;
    returnAddress.postcode = this.newAddressForm.value.postcode;
    returnAddress.city = this.newAddressForm.value.city;
    this.dialogRef.close(returnAddress);
  }

  /**
   * Abbrechen
   */
  onCancel() {
    this.dialogRef.close(false);
  }
}

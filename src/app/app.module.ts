import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RegistrationFormComponent} from './registration/registration-form/registration-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginFormComponent} from './login/login-form/login-form.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {UserdetailsComponent} from './userdetails/userdetails/userdetails.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HeaderComponent} from './header/header/header.component';
import {FooterAgbComponent} from './footer/footer-agb/footer-agb.component';
import {FooterCopyrightComponent} from './footer/footer-copyright/footer-copyright.component';
import {FooterPrivacyComponent} from './footer/footer-privacy/footer-privacy.component';
import {CategoryOverviewComponent} from './category-overview/category-overview.component';
import {HomeComponent} from './home/home/home.component';
import {DishComponent} from './dish/dish/dish.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {CartComponent} from './cart/cart.component';
import {MatIconModule} from '@angular/material/icon';
import {ImageUploadComponent} from './image-upload/image-upload.component';
import {NewDishComponent} from './admin/new-dish/new-dish.component';
import {NewSizeComponent} from './admin/new-size/new-size.component';
import {UserordersComponent} from './userdetails/userorders/userorders.component';
import {OrdersComponent} from './admin/orders/orders.component';
import {DatePipe} from '@angular/common';
import {HomeAboutusComponent} from './home/home-aboutus/home-aboutus.component';
import {CheckOutComponent} from './checkout/check-out/check-out.component';
import {NewAddressComponent} from './new-address/new-address.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {OrderItemsOverviewComponent} from './checkout/order-items-overview/order-items-overview.component';
import {MatRadioModule} from '@angular/material/radio';
import {NewCategoryComponent} from './admin/new-category/new-category.component';
import {EditDishComponent} from './admin/edit-dish/edit-dish.component';
import {MatListModule} from '@angular/material/list';
import { InvoiceComponent } from './invoice/invoice.component';
import { ConfigurationComponent } from './admin/configuration/configuration.component';
import { DailyClosingComponent } from './admin/daily-closing/daily-closing.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationFormComponent,
    LoginFormComponent,
    UserdetailsComponent,
    HeaderComponent,
    FooterAgbComponent,
    FooterCopyrightComponent,
    FooterPrivacyComponent,
    CategoryOverviewComponent,
    HomeComponent,
    DishComponent,
    CartComponent,
    ImageUploadComponent,
    NewDishComponent,
    NewSizeComponent,
    UserordersComponent,
    OrdersComponent,
    HomeAboutusComponent,
    CheckOutComponent,
    NewAddressComponent,
    OrderItemsOverviewComponent,
    NewCategoryComponent,
    EditDishComponent,
    InvoiceComponent,
    ConfigurationComponent,
    DailyClosingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    NoopAnimationsModule,
    MatButtonToggleModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatTabsModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {
}

/**
 * @author René Schiffner
 * Komponente invoice
 * Rechnung zu einer Bestellung im Frontend
 */

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import {Order} from '../_models/order';
import {Observable} from 'rxjs';
import {OrderService} from '../_services/order.service';
import {DatePipe} from '@angular/common';
import {DishService} from '../_services/dish.service';
import {Dish} from '../_models/dish';
import {Account} from '../_models/account';
import {first} from 'rxjs/operators';
import {AccountService} from '../_services/account.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {
  @ViewChild('content') content: ElementRef;
  private sub: any;
  private orderId: number;
  private ordersObservable: Observable<Order>;
  private accountObservable: Observable<Account>;
  order: Order;
  account: Account;
  date: string;
  dishes: Dish[];

  constructor(
    private route: ActivatedRoute,
    private orderService: OrderService,
    private dishService: DishService,
    private accountService: AccountService,
    public datePipe: DatePipe) { }

  /**
   * Initialisierung der Komponente
   * Hier wird die Bestellnummer abgerufen und die Daten zur Bestellung geholt
   */
  ngOnInit(): void {
    // get current order id
    this.sub = this.route.params.subscribe(params => {
      this.orderId = +params.id; // (+) converts string 'id' to a number
      console.log(this.orderId);
    });

    if (this.orderId != 0){
      this.getAccount();
      this.getOrder();
      this.getDishes();
    }
  }

  /**
   * Bestelldaten aus dem Backend holen
   */
  getOrder(){
    this.ordersObservable = this.orderService.getOrderById(this.orderId);
    this.ordersObservable.subscribe((response) => {
        console.log('order received');
        this.order = response;
      },
      error => {
        console.log(error);
      });
  }

  /**
   * alle Speisen aus dem Backend holen
   */
  getDishes(): void{
    this.dishService.getActiveDishes().subscribe((response) => {
        console.log('response received');
        this.dishes = response;
      },
      error => {
        // TODO Error handling
        console.log(error);
      });
  }

  /**
   * Benutzerdaten aus dem Backend holen
   */
  getAccount(): void{
    const token: string = JSON.parse(localStorage.getItem('sessionToken'));
    const username: string = JSON.parse(localStorage.getItem('username'));

    this.accountObservable = this.accountService.getUserDetails(username, token);
    // subscribe only one account
    this.accountObservable.pipe(first()).subscribe((data: Account) => {
        this.account = data;
      },
      error => {
        console.log(error);
      },
      () => {
        console.log('response received');
      });
  }

  /**
   * Speise und Größe ermitteln
   * @param dishId
   */
  getDishForOrderSize(dishId: number){
    if (dishId != null){
      const currentDish = this.dishes.filter(dish => dish.dishId === dishId);
      return currentDish[0].dish;
    }else{
      return 'Error';
    }
  }

  /**
   * Mwst zur aktuellen Bestellung
   * @param currentOrder
   */
  getMwst(currentOrder: Order){
    const sum = this.getSumOfOrder(currentOrder);
    return sum * (currentOrder.mwst);
  }

  /**
   * Gesamtsumme der Bestellung
   * @param order
   */
  getSumOfOrder(order: Order){
    let sum = 0;
    const orderSizes = order.order_sizes;
    for (const orderSize of orderSizes){
      const sumPerItem = (orderSize.amount * orderSize.pricePerItem);
      sum = sum + sumPerItem;
    }
    return sum;
  }

  /**
   * Konvertierung des Datums aus dem MySQL Format
   * @param currentOrder
   */
  convertDateTime(currentOrder: Order){
    this.date = this.datePipe.transform(currentOrder.date, 'dd.MM.yyyy');
    return this.date;
  }

  /**
   * Export des Containers content als PDF
   */
  exportAsPdf() {
    const data = document.getElementById('content');
    html2canvas(data).then(canvas => {
      const contentDataURL = canvas.toDataURL('image/png');
      const pdf = new jsPDF('p', 'cm', 'a4'); // Generates PDF in portrait mode
      pdf.addImage(contentDataURL, 'PNG', 0, 0.2 , 21.0, 29.7, '' , 'MEDIUM');
      pdf.save('Rechnung.pdf');
    });
  }
}

/**
 * @author René Schiffner
 * Komponente home
 * Startseite des Shops
 */
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  sliderImages = ['./assets/img/slider_pizza_600.jpg',
                  './assets/img/slider_pasta_600.jpg',
                  './assets/img/slider_salad_600.jpg' ];
  constructor() {
  }

  ngOnInit(): void {
  }

}

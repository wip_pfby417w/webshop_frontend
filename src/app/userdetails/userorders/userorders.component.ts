/**
 * @author René Schiffner
 * Komponente Useroders
 * Anzeige der Bestellungen zum Benutzer im Frontend
 */
import {Component, OnInit} from '@angular/core';
import {AccountService} from '../../_services/account.service';
import {Account} from '../../_models/account';
import {Observable} from 'rxjs';
import {first} from 'rxjs/operators';
import {Order} from '../../_models/order';
import {OrderService} from '../../_services/order.service';
import {DatePipe} from '@angular/common';
import {DishService} from '../../_services/dish.service';
import {Dish} from '../../_models/dish';
import {CartService} from '../../_services/cart.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-userorders',
  templateUrl: './userorders.component.html',
  styleUrls: ['./userorders.component.css']
})
export class UserordersComponent implements OnInit {
  orders: Order[];
  account: Account;
  dishes: Dish[];
  dish: string;
  userDetailObservable: Observable<Account>;
  userOrdersObservable: Observable<Order[]>;
  date: string;

  constructor(
    private orderService: OrderService,
    public accountService: AccountService,
    private dishService: DishService,
    public datePipe: DatePipe,
    private cartService: CartService,
    private router: Router
  ) {}

  /**
   * Initialisierung
   * Benutzer aus der Session lesen
   */
  ngOnInit(): void {
    const token: string = JSON.parse(localStorage.getItem('sessionToken'));
    const username: string = JSON.parse(localStorage.getItem('username'));

    this.userDetailObservable = this.accountService.getUserDetails(username, token);
    this.userDetailObservable.pipe(first()).subscribe((data: Account) => {
      this.account = data;
    },
    error => {
      console.log(error);
    },
    () => {
      console.log('user response received');
    });
    this.getOrders();
    this.getDishes();
  }

  /**
   * Bestellungen aus dem Backend lesen
   */
  getOrders(): void {
    // get orders for user
    const token: string = JSON.parse(localStorage.getItem('sessionToken'));
    const username: string = JSON.parse(localStorage.getItem('username'));

    this.userOrdersObservable = this.orderService.getOrdersForUser(username, token);
    this.userOrdersObservable.subscribe((response) => {
        console.log('orders received');
        this.orders = response;
      },
      error => {
        console.log(error);
      });
  }

  /**
   * alle Speisen aus dem Backend lesen
   */
  getDishes(): void{
    this.dishService.getActiveDishes().subscribe((response) => {
        console.log('response received');
        this.dishes = response;
      },
      error => {
        // TODO Error handling
        console.log(error);
      });
  }

  /**
   * Name der Speise und Größe ermitteln
   * @param dishId
   */
  getDishForOrderSize(dishId: number){
    if (dishId != null){
      const currentDish = this.dishes.filter(dish => dish.dishId === dishId);
      return currentDish[0].dish;
    }else{
      return 'Error';
    }
  }

  /**
   * Konvertierung der datetime aus dem MySQL Format in Date Format
   * @param actOrder
   */
  convertDateTime(actOrder: Order){
    this.date = this.datePipe.transform(actOrder.date, 'dd.MM.yyyy');
    return this.date;
  }

  /**
   * Status der Bestellung (Offen, In Bearbeitung und Abgeschlossen)
   * @param stateId
   */
  getState(stateId: number){
    switch (stateId) {
      case 1: {
        return '<span class="badge badge-secondary">Offen</span>';
        break;
      }
      case 2: {
        return '<span class="badge badge-warning">In Bearbeitung</span>';
        break;
      }
      case 3: {
        return '<span class="badge badge-success">Abgeschlossen</span>';
        break;
      }
    }
  }

  /**
   * Zahlungsart anzeigen (Rechnung, Bar bei Lieferung/Abholung)
   * @param paymentMethod
   */
  getPaymentMethod(paymentMethod: number){
    switch (paymentMethod) {
      case 1: {
        return '<li class="list-group-item">Rechnung</li>';
        break;
      }
      case 2: {
        return '<li class="list-group-item">Bar bei Lieferung</li>';
        break;
      }
      case 3: {
        return '<li class="list-group-item">Bar bei Abholung</li>';
        break;
      }
    }
  }

  /**
   * aktuelle Bestellung nochmal bestellen
   * @param order
   */
  orderAgain(order: Order): void{
    const orderSizes = order.order_sizes;
    for (const orderSize of orderSizes){
      const size = orderSize.size;
      const amount = orderSize.amount;
      this.cartService.addToCartWithAmount(size, amount);
    }
    window.alert('Artikel wurden in den Warenkorb gelegt!');
  }

  /**
   * Summe der Bestellung
   * @param order
   */
  getSumOfOrder(order: Order){
    let sum = 0;
    const orderSizes = order.order_sizes;
    for (const orderSize of orderSizes){
      const sumPerItem = (orderSize.amount * orderSize.pricePerItem);
      sum = sum + sumPerItem;
    }
    return sum;
  }

  /**
   * Rechnung anzeigen
   * @param orderId
   */
  showInvoice(orderId: number): void{
    this.router.navigate(['/user/order/', orderId]);
  }

}

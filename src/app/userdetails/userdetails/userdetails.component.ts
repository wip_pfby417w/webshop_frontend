/**
 * @author René Schiffner
 * Komponente Userdetails
 * Anzeige der Benutzerdaten im Frontend
 */

import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Account} from '../../_models/account';
import {AccountService} from '../../_services/account.service';
import {Observable} from 'rxjs';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.css']
})
export class UserdetailsComponent implements OnInit {
  userDetailsForm: FormGroup;
  isSubmitted = false;
  isSuccessful = false;
  isFailed = true;
  errorMessage = '';
  account: Account;
  userDetailObservable: Observable<Account>;

  constructor(private formBuilder: FormBuilder, private accountService: AccountService) {
  }

  /**
   * Initialisierung
   * Formular aufbauen und Benutzerdaten aus Backend laden
   */
  ngOnInit(): void {
    this.userDetailsForm = this.formBuilder.group({
        username: ['', {disabled: true}],
        password: ['', [Validators.required, Validators.minLength(6)]],
        email: ['', [Validators.required, Validators.email, Validators.maxLength(100)]],
        forename: ['', Validators.maxLength(50)],
        surname: [''],
        street: [''],
        housenumber: ['', Validators.maxLength(10)],
        postcode: ['', Validators.maxLength(10)],
        city: [''],
        phone: ['']
      }
    );
    this.getUserDetails();
  }

  /**
   * Helfermethode um die Controls auf dem einfacher aufzurufen
   */
  get userdetailsForm() {
    return this.userDetailsForm.controls;
  }

  /**
   * Daten zum aktuellen Benutzer (aus dem Session Token) lesen
   */
  getUserDetails(): void {
    const token: string = JSON.parse(localStorage.getItem('sessionToken'));
    const username: string = JSON.parse(localStorage.getItem('username'));
    const password = 'placeholder';
    this.userDetailObservable = this.accountService.getUserDetails(username, token);
    // subsribe only one account
    this.userDetailObservable.pipe(first()).subscribe((data: Account) => {
        this.account = data;
        this.accountService.setAccount(this.account);
        this.userDetailsForm.controls.username.setValue(this.account.username);
        this.userDetailsForm.controls.password.setValue(password);
        this.userDetailsForm.controls.email.setValue(this.account.email);
        this.userDetailsForm.controls.forename.setValue(this.account.forename);
        this.userDetailsForm.controls.surname.setValue(this.account.surname);
        this.userDetailsForm.controls.street.setValue(this.account.street);
        this.userDetailsForm.controls.housenumber.setValue(this.account.housenumber);
        this.userDetailsForm.controls.postcode.setValue(this.account.postcode);
        this.userDetailsForm.controls.city.setValue(this.account.city);
        this.userDetailsForm.controls.phone.setValue(this.account.phone);
      },
      error => {
        console.log(error);
      },
      () => {
        console.log('response received');
      });
  }

  /**
   * Bei Änderung der Benutzerdaten werden diese validiert und an das Backend übergeben
   */
  onSubmit(): void {
    this.isSubmitted = true;
    const token: string = JSON.parse(localStorage.getItem('sessionToken'));
    // change user details
    this.account.password = this.userDetailsForm.controls.password.value;
    this.account.email = this.userDetailsForm.controls.email.value;
    this.account.forename = this.userDetailsForm.controls.forename.value;
    this.account.surname = this.userDetailsForm.controls.surname.value;
    this.account.street = this.userDetailsForm.controls.street.value;
    this.account.housenumber = this.userDetailsForm.controls.housenumber.value;
    this.account.postcode = this.userDetailsForm.controls.postcode.value;
    this.account.city = this.userDetailsForm.controls.city.value;
    this.account.phone = this.userDetailsForm.controls.phone.value;
    console.log(this.account);

    this.accountService.setUserDetails(token, this.account).subscribe((data: Account) => {
        console.log(data);
        this.isSuccessful = true;
        setTimeout(() => {
          this.isSuccessful = false;
          this.isSubmitted = false;
        }, 3000);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isFailed = true;
      });
  }

  /**
   * Abbrechen der Änderung
   */
  onReset(): void {
    // reset form and get user details
    this.getUserDetails();
    this.isFailed = false;
    this.isSubmitted = false;
    this.isSuccessful = false;
    this.errorMessage = '';
  }
}

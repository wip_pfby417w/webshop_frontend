import {Component, ViewChild} from '@angular/core';
import {LoginFormComponent} from './login/login-form/login-form.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild(LoginFormComponent) myLoginForm: LoginFormComponent;

  title = 'Web-Shop Frontend';

  onReset(): void {
    this.myLoginForm.onReset();
  }
}

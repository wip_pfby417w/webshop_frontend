/**
 * @author Jonas Mertens
 * Komponente new size
 * Neue Größe für eine Speise hinzufügen
 */
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Size} from '../../_models/size';

@Component({
  selector: 'app-new-size',
  templateUrl: './new-size.component.html',
  styleUrls: ['./new-size.component.css']
})
export class NewSizeComponent implements OnInit {

  @Output() sizeChanged = new EventEmitter<Size>();
  @Input() size: Size;

  newSizeForm: FormGroup;
  isSubmitted = false;
  isSuccessful = false;

  constructor(
    private formBuilder: FormBuilder
  ) {
  }

  /**
   * Controls auf Formular
   */
  get sizeForm() {
    return this.newSizeForm.controls;
  }

  /**
   * Initialisierung
   */
  ngOnInit(): void {
    this.newSizeForm = this.formBuilder.group(
      {
        name: ['', [Validators.required, Validators.minLength(1)]],
        price: ['', Validators.min(0.1)]
      });

  }

  /**
   * Abbrechen
   */
  onCancel() {
    this.isSuccessful = false;
    this.isSubmitted = false;
    this.newSizeForm.reset();
  }


}

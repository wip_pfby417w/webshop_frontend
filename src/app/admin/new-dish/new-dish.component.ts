/**
 * @author Jonas Mertens
 * Komponente new dish
 * Neue Speise hinzufügen
 */
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Dish} from '../../_models/dish';
import {DishService} from '../../_services/dish.service';
import {CategoryService} from '../../_services/category.service';
import {Category} from '../../_models/category';
import {Size} from '../../_models/size';


@Component({
  selector: 'app-new-dish',
  templateUrl: './new-dish.component.html',
  styleUrls: ['./new-dish.component.css']
})
export class NewDishComponent implements OnInit {

  newDishForm: FormGroup;
  isSubmitted = false;
  isSuccessful = false;
  errorMessage = '';
  image: string;
  categories: Category[];
  sizes: Size[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private dishService: DishService,
    private categoryService: CategoryService) {
  }

  /**
   * Controls der Komponente
   */
  get dishForm() {
    return this.newDishForm.controls;
  }

  /**
   * Initialisierung des Formulars und aktive Kategorien ermitteln
   */
  ngOnInit(): void {
    this.newDishForm = this.formBuilder.group(
      {
        name: ['', [Validators.required, Validators.minLength(3)]],
        category: ['', Validators.required],
        toppings: [''],
        active: [''],
        image: ['']
      });
    this.categoryService.getCategories().subscribe((response) => {
      console.log('response received');
      this.categories = response;
    });

  }

  /**
   * Änderung bzw. neues Hinzufügen der Speise
   */
  onSubmit(): void {
    this.isSubmitted = true;

    // check registration is valid
    if (this.newDishForm.invalid) {
      return;
    }
    if (this.sizes.length == 0) {
      window.alert('You have to specify at least one size for every dish');
      return;
    }

    const dish: Dish = new Dish();
    dish.dish = this.newDishForm.get('name').value;
    dish.categoryId = this.newDishForm.get('category').value;
    dish.toppings = this.newDishForm.get('toppings').value;
    dish.active = this.newDishForm.get('active').value;
    dish.picture = this.image;

    let dishId: number;
    // POST Dish and receive response
    this.dishService.createNew(dish).subscribe(data => {
        console.log('created dishId' + data);
        dishId = data;
        this.sizes.forEach(element => {
          element.dishId = dishId;
          this.dishService.createNewSize(element).subscribe(data => {
              console.log(data);
              this.isSuccessful = true;
            },
            err => {
              window.alert(err.error.message);
              this.isSuccessful = false;
            }
          );
        });
      },
      err => {
        window.alert(err.error.message);
        this.isSuccessful = false;
      });


  }

  /**
   * Abbrechen
   */
  onCancel() {
    this.isSuccessful = false;
    this.isSubmitted = false;
    this.sizes = [];
    this.image = null;
    this.newDishForm.reset();

  }

  /**
   * Bild hinzufügen
   * @param image
   */
  onImageSelect(image: any) {
    this.image = image;
  }

  /**
   * Größe zur Speise hinzufügen
   */
  addSize() {
    const newSize = new Size();
    newSize.size = 'Neue Größe';
    newSize.price = 0;
    this.sizes.push(newSize);
  }

  /**
   * Größe der Speise geändert
   * @param size
   * @param event
   */
  sizeChanged(size: Size, event: Size) {
    const index = this.sizes.findIndex(element => element = size);
    this.sizes[index] = event;
  }

  /**
   * Abbrechen der Änderung der Größe
   * @param sizeToDelte
   */
  onCancelSize(sizeToDelte: Size) {
    const index = this.sizes.findIndex(element => element == sizeToDelte);
    this.sizes.splice(index, 1);
    this.sizes;
  }
}

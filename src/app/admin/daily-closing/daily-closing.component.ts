/**
 * @author René Schiffner
 * Komponente daily closing
 * Tagesabschluss im Frontend
 */
import { Component, OnInit } from '@angular/core';
import {MatDatepickerInputEvent, MatDatepickerModule} from '@angular/material/datepicker';
import {Observable} from 'rxjs';
import {DailyClosing} from '../../_models/daily-closing';
import {DailyClosingService} from '../../_services/daily-closing.service';
import {DatePipe} from '@angular/common';
import {SoldDishes} from '../../_models/sold-dishes';
import {ConfigService} from '../../_services/config.service';

@Component({
  selector: 'app-daily-closing',
  templateUrl: './daily-closing.component.html',
  styleUrls: ['./daily-closing.component.css']
})
export class DailyClosingComponent implements OnInit {

  isSuccessful: boolean;
  isSelected: boolean;
  maxDate: Date;
  selectedDate: Date;
  closingObservable: Observable<DailyClosing>;
  dailyClosing: DailyClosing;
  displayDate: string;
  soldDishes: SoldDishes[];

  constructor(private dailyClosingService: DailyClosingService,
              public datePipe: DatePipe,
              private configService: ConfigService) { }

  /**
   * Initialisierung (Max-Datum setzen und ansonsten auf gestrigen Tag setzen)
   */
  ngOnInit(): void {
    // Set the max to the current day
    const currentYear = new Date().getFullYear();
    const currentDay = new Date().getDate() - 1;
    const currentMonth = new Date().getMonth();
    this.maxDate = new Date(currentYear, currentMonth, currentDay);
    this.selectedDate = this.maxDate;
  }

  /**
   * Wert des Datums mit Datepicker geändert
   * @param type
   * @param event
   */
  changeDate(type: string, event: MatDatepickerInputEvent<Date>) {
    this.selectedDate = event.value;
    this.displayDate = this.datePipe.transform(this.selectedDate, 'dd.MM.yyyy');
    this.isSelected = false;
  }

  /**
   * Tagesabschluss holen
   */
  selectClosing(): void{
    this.isSelected = true;
    this.displayDate = this.datePipe.transform(this.selectedDate, 'dd.MM.yyyy');
    this.getClosing();
  }

  /**
   * Tagesabschluss aus dem Backend ermitteln / falls nicht vorhanden
   * wird dies im Backend automatisch angelegt
   */
  getClosing(): void{
    const token: string = JSON.parse(localStorage.getItem('sessionToken'));

    const date = this.datePipe.transform(this.selectedDate, 'yyyy-MM-dd');

    this.closingObservable = this.dailyClosingService.getClosing(date, token);
    this.closingObservable.subscribe((response) => {
        console.log('closing received');
        this.dailyClosing = response;
        // this.isSuccessful = true;
      },
      error => {
        console.log(error);
      });

    if (this.dailyClosing != null){
    }
  }

  /**
   * verkaufte Speisen zum Tagesabschluss
   */
  getSoldDishes(){
    this.soldDishes = this.dailyClosing.soldDishes;
    return this.soldDishes;
  }

  /**
   * Tagesabschluss löschen (auch im Backend)
   */
  deleteClosing(): void{
    const token: string = JSON.parse(localStorage.getItem('sessionToken'));
    const date = this.datePipe.transform(this.selectedDate, 'yyyy-MM-dd');
    this.closingObservable = this.dailyClosingService.deleteClosing(date);
    this.closingObservable.subscribe((response) => {
        console.log('closing received');
        this.dailyClosing = response;
        this.isSelected = false;
        // this.isSuccessful = true;
      },
      error => {
        console.log(error);
      });
  }


}

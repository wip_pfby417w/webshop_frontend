import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyClosingComponent } from './daily-closing.component';

describe('DailyClosingComponent', () => {
  let component: DailyClosingComponent;
  let fixture: ComponentFixture<DailyClosingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyClosingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyClosingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

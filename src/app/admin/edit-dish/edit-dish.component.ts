/**
 * @author Jonas Mertens
 * Komponente new category
 * Speise editieren
 */
import {Component, OnInit} from '@angular/core';
import {Dish} from '../../_models/dish';
import {DishService} from '../../_services/dish.service';
import {Category} from '../../_models/category';
import {Size} from '../../_models/size';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryService} from '../../_services/category.service';

@Component({
  selector: 'app-edit-dish',
  templateUrl: './edit-dish.component.html',
  styleUrls: ['./edit-dish.component.css']
})
export class EditDishComponent implements OnInit {

  allDishes: Dish[];
  selectedDish: Dish = new Dish();
  isSuccessful: boolean;
  categories: Category[];
  isSubmitted: boolean;
  editDishForm: FormGroup;
  image: string;
  sizesToDelete: Size[] = [];

  constructor(
    private dishService: DishService,
    private formBuilder: FormBuilder,
    private categoryService: CategoryService
  ) {
  }

  /**
   * Controls vom Formular holen
   */
  get dishForm() {
    return this.editDishForm.controls;
  }

  /**
   * Initialisierung des Formulars
   * Kategorien und Speisen aus Backend holen
   */
  ngOnInit(): void {
    this.editDishForm = this.formBuilder.group(
      {
        name: ['', [Validators.required, Validators.minLength(3)]],
        category: ['', Validators.required],
        toppings: [''],
        active: [''],
        image: ['']
      });
    this.categoryService.getCategories().subscribe((response) => {
      console.log('response received');
      this.categories = response;
    });

    this.dishService.getAllDishes().subscribe(data => {
      console.log('Got ' + data.length + ' Dished as AllDishes');
      this.allDishes = data;
    }, error => {
      window.alert(error.error.message);
    });
  }

  /**
   * Ausgewählte Speise
   * Formularfelder entsprechend befüllen
   * @param dish
   */
  selectDish(dish: Dish) {
    this.selectedDish = dish;
    this.prefillControls();
  }

  /**
   * Formularfelder befüllen
   */
  prefillControls() {
    this.editDishForm.controls.name.setValue(this.selectedDish.dish);
    this.editDishForm.controls.category.setValue(this.selectedDish.categoryId);
    this.editDishForm.controls.toppings.setValue(this.selectedDish.toppings);
    this.editDishForm.controls.active.setValue(this.selectedDish.active);
  }

  /**
   * Abschicken der Änderung
   */
  onSubmit() {
    this.isSubmitted = true;
    // check registration is valid
    if (this.editDishForm.invalid) {
      return;
    }
    if (this.selectedDish.sizes.length == 0) {
      window.alert('You have to specify at least one size for every dish');
      return;
    }
    // if registration is valid
    let dishId: number;
    // POST Dish and receive response
    this.dishService.editDish(this.selectedDish).subscribe(data => {
        console.log('edited dishId' + data);
        this.selectedDish.sizes.forEach(element => {
          // Size already exists in Backend. It has to be updated
          if (element.sizeId != null) {
            this.dishService.editSize(element).subscribe(data => {
              console.log('Updated Size with ID ' + data);
              this.isSuccessful = true;
            }, error => {
              window.alert(error.error.message);
              this.isSuccessful = false;
            });
          } else { // Size doesnt exist in backend
            element.dishId = this.selectedDish.dishId;
            this.dishService.createNewSize(element).subscribe(data => {
                console.log(data);
                this.isSuccessful = true;
              },
              err => {
                window.alert(err.error.message);
                this.isSuccessful = false;
              }
            );
          }
        });
        this.sizesToDelete.forEach(element => {
          this.dishService.deleteSize(element).subscribe(data => {
            console.log('Delete Size with ID ' + element.sizeId);
            this.isSuccessful = true;
          }, error => {
            window.alert(error.error.message);
            this.isSuccessful = false;
          });
        });
      },
      err => {
        window.alert(err.error.message);
        this.isSuccessful = false;
      });


  }

  /**
   * Abbrechen der Änderung
   */
  onCancel() {
    this.isSuccessful = false;
    this.isSubmitted = false;
    this.image = null;
    this.editDishForm.reset();
    this.selectedDish = new Dish();
    this.sizesToDelete = [];
  }

  /**
   * Bild auswählen
   * @param image
   */
  onImageSelect(image: any) {
    this.image = image;
  }

  /**
   * Änderung der Größe
   * @param size
   * @param event
   */
  sizeChanged(size: Size, event: Size) {
    const index = this.selectedDish.sizes.findIndex(element => element = size);
    this.selectedDish.sizes[index] = event;
  }

  /**
   * Abbrechen
   * @param sizeToDelte
   */
  onCancelSize(sizeToDelte: Size) {
    const index = this.selectedDish.sizes.findIndex(element => element == sizeToDelte);
    this.sizesToDelete.push(this.selectedDish.sizes[index]);
    this.selectedDish.sizes.splice(index, 1);
    this.selectedDish.sizes;
  }

  /**
   * Größe zu Speise hinzufügen
   */
  addSize() {
    const newSize = new Size();
    newSize.size = 'Neue Größe';
    newSize.price = 0;
    this.selectedDish.sizes.push(newSize);
  }
}

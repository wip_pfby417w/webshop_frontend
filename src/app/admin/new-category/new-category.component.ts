/**
 * @author Jonas Mertens
 * Komponente new category
 * Neue Kategorie hinzufügen
 */
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DishService} from '../../_services/dish.service';
import {CategoryService} from '../../_services/category.service';
import {Category} from '../../_models/category';

@Component({
  selector: 'app-new-category',
  templateUrl: './new-category.component.html',
  styleUrls: ['./new-category.component.css']
})
export class NewCategoryComponent implements OnInit {
  newCategoryForm: FormGroup;
  isSubmitted = false;
  isSuccessful = false;

  constructor(
    private formBuilder: FormBuilder,
    private dishService: DishService,
    private categoryService: CategoryService
  ) {
  }

  get categoryForm() {
    return this.newCategoryForm.controls;
  }

  /**
   * Initialisierung
   */
  ngOnInit(): void {
    this.newCategoryForm = this.formBuilder.group(
      {
        name: ['', [Validators.required, Validators.minLength(3)]],
        active: ['']
      });

  }

  /**
   * Neue Kategorie an Backend übergeben
   */
  onSubmit() {
    this.isSubmitted = true;

    // check newCateogry is valid
    if (this.newCategoryForm.invalid) {
      return;
    }

    // if Category is valid, build Category Object
    const newCategory: Category = new Category();
    newCategory.categoryName = this.newCategoryForm.value.name;
    newCategory.active = this.newCategoryForm.value.active;

    // POST Category and receive response
    this.categoryService.createNew(newCategory).subscribe(data => {
        console.log('created Category' + data);
        this.isSuccessful = true;

      },
      err => {
        window.alert(err.error.message);
        this.isSuccessful = false;
      });
  }

  /**
   * Abbrechen
   */
  onCancel() {
    this.isSuccessful = false;
    this.isSubmitted = false;
    this.newCategoryForm.reset();
  }
}

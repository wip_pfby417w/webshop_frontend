/**
 * @author René Schiffner
 * Komponente orders
 * Bestellungen im Frontend allerdings nur als Admin sichtbar
 */
import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {Order} from '../../_models/order';
import {Account} from '../../_models/account';
import {Dish} from '../../_models/dish';
import {Observable} from 'rxjs';
import {OrderService} from '../../_services/order.service';
import {AccountService} from '../../_services/account.service';
import {DishService} from '../../_services/dish.service';
import {DatePipe} from '@angular/common';
import {first} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})

export class OrdersComponent implements OnInit {

  order: Order;
  orders: Order[];
  filteredOrders: Order[];
  account: Account;
  dishes: Dish[];
  dish: string;
  userDetailObservable: Observable<Account>;
  OrdersObservable: Observable<Order[]>;
  date: string;
  private sub: any;
  stateId: number;

  constructor(
    private orderService: OrderService,
    private accountService: AccountService,
    private dishService: DishService,
    public datePipe: DatePipe,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  /**
   * Initialisierung
   * Holt alle Bestellungen und Speisen
   */
  ngOnInit(): void {
    const token: string = JSON.parse(localStorage.getItem('sessionToken'));
    const username: string = JSON.parse(localStorage.getItem('username'));

    this.userDetailObservable = this.accountService.getUserDetails(username, token);
    this.userDetailObservable.pipe(first()).subscribe((data: Account) => {
        this.account = data;
      },
      error => {
        console.log(error);
      },
      () => {
        console.log('user response received');
      });

    // get parameter stateId
    this.sub = this.route.params.subscribe(params => {
      this.stateId = +params['stateId'];
    });

    this.getOrders();
    this.getDishes();
  }

  /**
   * Holt alle Bestellungen aus dem Backend
   */
  getOrders(): void {
    // get orders for user
    const token: string = JSON.parse(localStorage.getItem('sessionToken'));
    const username: string = JSON.parse(localStorage.getItem('username'));

    this.OrdersObservable = this.orderService.getAllOrders(token);
    this.OrdersObservable.subscribe((response) => {
        console.log('orders received');
        this.orders = response;
        this.filteredOrders = this.orders;
      },
      error => {
        console.log(error);
      });
  }

  /**
   * ermittelt alle aktiven Speisen aus dem Backend
   */
  getDishes(): void{
    this.dishService.getActiveDishes().subscribe((response) => {
        console.log('response received');
        this.dishes = response;
      },
      error => {
        // TODO Error handling
        console.log(error);
      });
  }

  /**
   * Name der Speise ermitteln
   * @param dishId
   */
  getDishForOrderSize(dishId: number){
    if (dishId != null){
      const currentDish = this.dishes.filter(dish => dish.dishId === dishId);
      return currentDish[0].dish;
    }else{
      return 'Error';
    }
  }

  /**
   * Datumskonvertierung aus MySQL Datetime in lesbares Format
   * @param actOrder
   */
  convertDateTime(actOrder: Order){
    this.date = this.datePipe.transform(actOrder.date, 'dd.MM.yyyy');
    return this.date;
  }

  /**
   * Filterfunktion nach Status der Bestellung (Offen, In Bearbeitung, ...)
   * @param stateId
   */
  getOrdersByState(stateId: number){
    if (stateId != 0) {
      this.filteredOrders = this.orders.filter(order => order.stateId === stateId);
      this.filteredOrders.sort((a, b) => a.orderId > b.orderId ? -1 : 0)
      return this.filteredOrders;
    }else{
      this.filteredOrders = this.orders;
      this.filteredOrders.sort((a, b) => a.orderId > b.orderId ? -1 : 0)
      return this.filteredOrders;
    }
  }

  /**
   * Status der Bestellung als Badge anzeigen
   * @param stateId
   */
  getState(stateId: number){
    switch (stateId) {
      case 1: {
        return '<span class="badge badge-secondary">Offen</span>';
        break;
      }
      case 2: {
        return '<span class="badge badge-warning">In Bearbeitung</span>';
        break;
      }
      case 3: {
        return '<span class="badge badge-success">Abgeschlossen</span>';
        break;
      }
    }
  }

  /**
   * Prüfen ob Bestellungen vorliegen
   */
  checkOrdersNotEmpty(){
    if (this.orders == null){
      return false;
    }else{ return true; }
  }

  /**
   * Falls Benutzer kein Admin ist soll Fehlermeldung ausgegeben werden
   */
  checkIsAdmin(){
    const isAdmin = this.accountService.isAdmin();
    if (!isAdmin){
      setTimeout(() => { this.router.navigate(['']); }, 3000 );
    }
    return isAdmin;
  }

  /**
   * Status der Bestellung ändern
   * @param order
   * @param stateId
   */
  changeOrderState(order: Order, stateId: number){
    order.stateId = stateId;
    this.orderService.setOrderState(order).subscribe(
        (data: Order) => { console.log(data); },
        err => { console.log(err); });
  }

  /**
   * Zahlungsart für Admin ermitteln
   * @param paymentMethod
   */
  getPaymentMethod(paymentMethod: number){
    switch (paymentMethod) {
      case 1: {
        return '<li class="list-group-item">Rechnung</li>';
        break;
      }
      case 2: {
        return '<li class="list-group-item">Bar bei Lieferung</li>';
        break;
      }
      case 3: {
        return '<li class="list-group-item">Bar bei Abholung</li>';
        break;
      }
    }
  }

  /**
   * Summe der Bestellung
   * @param order
   */
  getSumOfOrder(order: Order){
    let sum = 0;
    const orderSizes = order.order_sizes;
    for (const orderSize of orderSizes){
      const sumPerItem = (orderSize.amount * orderSize.pricePerItem);
      sum = sum + sumPerItem;
    }
    return sum;
  }

  /**
   * Rechnung anzeigen
   * @param orderId
   */
  showInvoice(orderId: number): void{
    this.router.navigate(['/admin/invoice/', orderId]);
  }

}

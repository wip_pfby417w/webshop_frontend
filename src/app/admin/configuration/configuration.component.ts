/**
 * @author René Schiffner
 * Komponente configuration
 * Konfigurationen im Frontend anzeigen und ändern
 */
import { Component, OnInit } from '@angular/core';
import {ConfigService} from '../../_services/config.service';
import {Observable} from 'rxjs';
import {Config} from '../../_models/config';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {

  configObservable: Observable<any>;
  configs: Config[];
  selectedConfig: Config = new Config();
  editConfigForm: FormGroup;
  isSubmitted: boolean;
  isSuccessful: boolean;

  constructor(private configService: ConfigService,
              private formBuilder: FormBuilder) { }

  /**
   * Initialisierung des Formulars
   */
  ngOnInit(): void {
    this.editConfigForm = this.formBuilder.group(
      {
        name: ['', [Validators.required, Validators.minLength(3)]],
        desc: ['', Validators.required],
        val: [''],
        active: [''],
      });
    this.getConfiguration();
  }

  /**
   * Konfiguration aus Backend holen
   */
  getConfiguration(): void{
    this.configObservable = this.configService.getConfiguration();
    this.configObservable.subscribe((response) => {
        console.log('order received');
        this.configs = response;
      },
      error => {
        console.log(error);
      });
  }

  /**
   * ausgewählte Konfiguration und entsprechnde Daten dazu in
   * die Formularfelder füllen
   * @param config
   */
  selectConfig(config: Config){
    this.selectedConfig = config;
    this.preFillControls();
  }

  preFillControls() {
    this.editConfigForm.controls.name.setValue(this.selectedConfig.configName);
    this.editConfigForm.controls.desc.setValue(this.selectedConfig.configDesc);
    this.editConfigForm.controls.val.setValue(this.selectedConfig.configValue);
    this.editConfigForm.controls.active.setValue(this.selectedConfig.active);
  }

  /**
   * Speichern der Konfiguration
   */
  onSubmit() {
    this.isSubmitted = true;

    if (this.editConfigForm.invalid) {
      return;
    }
    this.selectedConfig.configName = this.editConfigForm.controls.name.value;
    this.selectedConfig.configDesc = this.editConfigForm.controls.desc.value;
    this.selectedConfig.configValue = this.editConfigForm.controls.val.value;
    this.selectedConfig.active = this.editConfigForm.controls.active.value;

    this.configService.editConfig(this.selectedConfig).subscribe(data => {
      console.log('edited config' + data);
      this.isSuccessful = true;
    }, err => {
      window.alert(err.error.message);
      this.isSuccessful = false;
    });

  }
}

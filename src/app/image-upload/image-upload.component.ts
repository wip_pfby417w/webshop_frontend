/**
 * @author Jonas Mertens
 * Komponente image upload
 * Upload eines Bildes aus Frontend ins Backend
 */

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImageService} from '../_services/image.service';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {

  @Output() imageSelectedEvent = new EventEmitter<string>();
  @Input() image: any;

  constructor(
    private imageService: ImageService
  ) {
  }

  ngOnInit(): void {
  }

  /**
   * Upload des Bildes
   * @param files
   */
  handleFileInput(files: any) {
    this.image = files[0];
    let reader = new FileReader();
    reader.onload = (e: any) => {
      this.image = e.target.result;

      this.imageService.compressImageToLess1Mb(this.image).then(element => {
        this.image = element;
        this.imageSelectedEvent.emit(this.image);
      });
    };
    reader.readAsDataURL(files[0]);
  }
}

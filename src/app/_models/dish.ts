import {Size} from './size';
/**
 * @author Jonas Mertens
 * Klasse Dish
 */
export class Dish {
  dishId: number;
  categoryId: number;
  dish: string;
  toppings: string;
  active: boolean;
  sizes: Size[];
  picture: string;
}

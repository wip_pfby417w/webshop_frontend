import {Size} from './size';
/**
 * @author Jonas Mertens
 * Klasse OrderSizes
 */
export class OrderSizes {
  orderId: number;
  amount: number;
  pricePerItem: number;
  size: Size;
  sizeId: number;

}

/**
 * @author Jonas Mertens
 * Klasse Category
 */
export class Category {
  categoryId: number;
  categoryName: string;
  active: boolean;
}

/**
 * @author René Schiffner
 * Klasse SoldDishes
 */
export class SoldDishes {
  dishId: number;
  dishName: string;
  amount: number;
}

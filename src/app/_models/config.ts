/**
 * @author René Schiffner
 * Klasse Config
 */
export class Config {
  configId: number;
  configName: string;
  configValue: any;
  configDesc: string;
  active: boolean;
}

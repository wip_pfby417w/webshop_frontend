/**
 * @author Jonas Mertens
 * Klasse Address
 */
export class Address {
  forename: string;
  surname: string;
  street: string;
  housenumber: string;
  postcode: number;
  city: string;

  constructor() {
    this.forename = '';
    this.surname = '';
    this.street = '';
    this.housenumber = '';
    this.postcode = 0;
    this.city = '';
  }
}

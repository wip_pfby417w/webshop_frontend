import {SoldDishes} from './sold-dishes';
/**
 * @author René Schiffner
 * Klasse DailyClosing
 */
export class DailyClosing {
  closingId: number;
  closingDate: Date;
  orderCount: number;
  dishCount: number;
  earning: number;
  soldDishes: SoldDishes[];
  mwst: number;
}

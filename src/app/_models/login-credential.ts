/**
 * @author Jonas Mertens
 * Klasse LoginCredential
 */
export class LoginCredential {
  username: string;
  password: string;
}

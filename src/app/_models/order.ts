import {OrderSizes} from './order-sizes';
import {Address} from './address';

/**
 * @author René Schiffner
 * Klasse Order
 */
export class Order {
  orderId: number;
  accountId: number;
  date: string;
  stateId: number;
  order_sizes: OrderSizes[];
  shippingAddressId: number;
  invoiceAddressId: number;
  shippingAddress: Address;
  invoiceAddress: Address;
  paymentMethod: number;
  deliveryOption: number;
  mwst: number;
}

/**
 * @author Jonas Mertens
 * Klasse JwtTokenPayload
 */
export class JwtTokenPayload {
  sub: string;
  ROLES: string;
  exp: number;
  iat: number;
}

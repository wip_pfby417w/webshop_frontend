/**
 * @author René Schiffner
 * Klasse Account
 */
export class Account {
  // model the structure of an account
  accountId: number;
  username: string;
  password: string;
  email: string;
  forename: string;
  surname: string;
  street: string;
  housenumber: string;
  postcode: number;
  city: string;
  phone: string;
  groupId: number;
}

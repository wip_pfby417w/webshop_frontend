/**
 * @author Jonas Mertens
 * Klasse Size
 */
export class Size {
  sizeId: number;
  dishId: number;
  size: string;
  price: number;

  compare(a, b) {
    if (a.price < b.price) {
      return -1;
    }
    if (a.price > b.price) {
      return 1;
    }
    return 0;
  }
}

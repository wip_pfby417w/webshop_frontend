/**
 * @author René Schiffner
 * Komponente Registration
 * Registrierung eines Benutzers im Frontend
 */
import {Component, OnInit} from '@angular/core';
import {Account} from '../../_models/account';
import {AccountService} from '../../_services/account.service';
import {Match} from '../../_helpers/match.validator';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})

export class RegistrationFormComponent implements OnInit {
  registrationForm: FormGroup;
  isSubmitted = false;
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  account: Account;
  accounts: Account[];

  accountsObservable: Observable<Account[]>;

  constructor(private formBuilder: FormBuilder, private accountService: AccountService) {
  }

  /**
   * Initialisierung des Formulars
   */
  ngOnInit(): void {

    this.registrationForm = this.formBuilder.group(
      {
        username: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirm_password: ['', Validators.required],
        email: ['', [Validators.required, Validators.email, Validators.maxLength(100)]],
        forename: ['', Validators.maxLength(50)],
        surname: [''],
        street: [''],
        housenumber: ['', Validators.maxLength(10)],
        postcode: ['', Validators.maxLength(10)],
        city: [''],
        phone: ['']
      },
      // custom validator to check passwords
      {validator: Match('password', 'confirm_password')}
    );
  }

  // get controls of registration form
  get regForm() {
    return this.registrationForm.controls;
  }


  /**
   * Regisitrierung durchführen und Validierung durchlaufen
   */
  onSubmit(): void {
    this.isSubmitted = true;

    // check registration is valid
    if (this.registrationForm.invalid) {
      return;
    }
    // if registration is valid
    this.account = new Account();
    this.account.username = this.registrationForm.get('username').value;

    // get password and confirm password to check -> validation through form
    this.account.password = this.registrationForm.get('password').value;

    this.account.email = this.registrationForm.get('email').value;
    this.account.forename = this.registrationForm.get('forename').value;
    this.account.surname = this.registrationForm.get('surname').value;
    this.account.street = this.registrationForm.get('street').value;
    this.account.housenumber = this.registrationForm.get('housenumber').value;
    this.account.postcode = this.registrationForm.get('postcode').value;
    this.account.city = this.registrationForm.get('city').value;
    this.account.phone = this.registrationForm.get('phone').value;

    console.log(this.account);

    // register account and receive response
    this.accountService.register(this.account).subscribe(data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      });
  }

  /**
   * Registrierung abbrechen
   */
  onCancel() {
    this.isSuccessful = false;
    this.isSubmitted = false;
    this.isSignUpFailed = false;
    this.registrationForm.reset();
  }
}

/**
 * @author René Schiffner
 * Komponente dish
 * Zur Zeit keine Verwendung, sollte als Detailseite dienen
 */
import {Component, OnInit} from '@angular/core';
import {Dish} from '../../_models/dish';
import {DishService} from '../../_services/dish.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.css']
})
export class DishComponent implements OnInit {
  dish: Dish;
  dishId: number;
  isLoaded = false;
  private sub: any;

  constructor(private dishService: DishService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    // get current dish id
    this.sub = this.route.params.subscribe(params => {
      this.dishId = +params['id']; // (+) converts string 'id' to a number
      console.log(this.dishId);
    });

    // now get dish from database
    this.dishService.getDish(this.dishId).subscribe((response) => {
      console.log('response received');
      this.dish = response;
      this.isLoaded = true;
    });
  }
}

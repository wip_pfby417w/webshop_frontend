import {FormGroup} from '@angular/forms';

/**
 * @author René Schiffner
 * Helferklasse für Validation von Feldern
 * @param controlName
 * @param matchingControlName
 * @constructor
 */
// validator to check that two fields match
export function Match(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const firstControl = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (firstControl.value !== matchingControl.value) {
      matchingControl.setErrors({mustMatch: true});
    } else {
      matchingControl.setErrors(null);
    }
  };
}

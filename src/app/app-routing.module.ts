import {NgModule} from '@angular/core';
import {LoginFormComponent} from './login/login-form/login-form.component';
import {RegistrationFormComponent} from './registration/registration-form/registration-form.component';
import {RouterModule, Routes} from '@angular/router';
import {UserdetailsComponent} from './userdetails/userdetails/userdetails.component';
import {FooterAgbComponent} from './footer/footer-agb/footer-agb.component';
import {FooterCopyrightComponent} from './footer/footer-copyright/footer-copyright.component';
import {FooterPrivacyComponent} from './footer/footer-privacy/footer-privacy.component';
import {CategoryOverviewComponent} from './category-overview/category-overview.component';
import {HomeComponent} from './home/home/home.component';
import {DishComponent} from './dish/dish/dish.component';
import {ImageUploadComponent} from './image-upload/image-upload.component';
import {NewDishComponent} from './admin/new-dish/new-dish.component';
import {UserordersComponent} from './userdetails/userorders/userorders.component';
import {OrdersComponent} from './admin/orders/orders.component';
import {HomeAboutusComponent} from './home/home-aboutus/home-aboutus.component';
import {CheckOutComponent} from './checkout/check-out/check-out.component';
import {NewCategoryComponent} from './admin/new-category/new-category.component';
import {EditDishComponent} from './admin/edit-dish/edit-dish.component';
import {InvoiceComponent} from './invoice/invoice.component';
import {ConfigurationComponent} from './admin/configuration/configuration.component';
import {DailyClosingComponent} from './admin/daily-closing/daily-closing.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'about-us', component: HomeAboutusComponent},
  {path: 'login', component: LoginFormComponent},
  {path: 'register', component: RegistrationFormComponent},
  {path: 'user/details', component: UserdetailsComponent},
  {path: 'user/orders', component: UserordersComponent},
  {path: 'user/order/:id', component: InvoiceComponent},
  {path: 'agb', component: FooterAgbComponent},
  {path: 'impressum', component: FooterCopyrightComponent},
  {path: 'datenschutz', component: FooterPrivacyComponent},
  {path: 'category-overview', component: CategoryOverviewComponent},
  {path: 'category-overview/:cat', component: CategoryOverviewComponent},
  {path: 'dish/:id', component: DishComponent},
  {path: 'image-upload', component: ImageUploadComponent},
  {path: 'admin/new-dish', component: NewDishComponent},
  {path: 'admin/orders', component: OrdersComponent},
  {path: 'admin/invoice/:id', component: InvoiceComponent, pathMatch: 'full'},
  {path: 'checkOut', component: CheckOutComponent},
  {path: 'admin/new-category', component: NewCategoryComponent},
  {path: 'admin/edit-dish', component: EditDishComponent},
  {path: 'admin/config', component: ConfigurationComponent},
  {path: 'admin/closing', component: DailyClosingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

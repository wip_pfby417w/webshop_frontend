import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Dish} from '../_models/dish';
import {BackendService} from './backend.service';
import {Size} from "../_models/size";

@Injectable({
  providedIn: 'root'
})
export class DishService {
  activeDishes;
  allDishes;

  constructor(private backendService: BackendService) {
  }

  getDish(dishId: number): Observable<Dish> {
    return this.backendService.get('/dish/' + dishId);
  }

  getActiveDishes(): Observable<Dish[]> {
    return this.backendService.get('/dish/listActiveDishes');
  }

  createNew(dish: Dish): Observable<any> {
    const body = JSON.stringify(dish);
    return this.backendService.newDish(body);
  }

  createNewSize(size: Size): Observable<any> {
    const body = JSON.stringify(size);
    return this.backendService.newSize(body);
  }

  getAllDishes(): Observable<Dish[]> {
    return this.backendService.get('/dish/list');
  }

  editDish(dish: Dish): Observable<any> {
    const body = JSON.stringify(dish);
    return this.backendService.put('/dish', body);
  }

  editSize(size: Size): Observable<any> {
    const body = JSON.stringify(size);
    return this.backendService.put('/size', body);
  }

  deleteSize(size: Size): Observable<any> {
    const body = JSON.stringify(size);
    return this.backendService.delete('/size', body);
  }
}

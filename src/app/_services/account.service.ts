import {Injectable} from '@angular/core';
import {Account} from '../_models/account';
import {BackendService} from './backend.service';
import {Observable} from 'rxjs';
import {JwtTokenPayload} from '../_models/jwt-token-payload';
import {Address} from "../_models/address";

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private roles: string;
  private username = '';
  private token: string;
  private account: Account;

  constructor(private backendService: BackendService) {
    if (this.validateCachedJwt()) {
      this.getRolesFromCache();
      this.getTokenFromCache();
    }
  }

  getAccounts(): Observable<any> {
    return this.backendService.request('GET', '/admin/getAccounts', {});
  }

  getUserDetails(username: string, token: string): Observable<any> {
    // TODO Error handling
    return this.backendService.get('/user/details/', {account: username});
  }

  register(account: Account): Observable<any> {
    const body = JSON.stringify(account);
    return this.backendService.register(body);
  }

  setUserDetails(token: string, account: Account): Observable<any> {
    // TODO Error handling
    const body = JSON.stringify(account);
    return this.backendService.request('POST', '/user/change/', body, token);
  }

  parseJwt(token): JwtTokenPayload {
    if (token != null) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      const jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      }).join(''));

      return JSON.parse(jsonPayload);
    }
  }

  validateCachedJwt() {
    const cachedToken = localStorage.getItem('sessionToken');
    const cachedUser = JSON.parse(localStorage.getItem('username'));
    // check if token is null
    if (cachedToken != null) {
      const payload: JwtTokenPayload = this.parseJwt(JSON.parse(cachedToken));
      const expTime = payload.exp * 1000; // Multiplicate with 1000 due to incompatibility
      const issuedAtTime = payload.iat * 1000; // Multiplicate with 1000 due to incompatibility
      const date = Date.now();
      if (payload.sub == cachedUser && expTime > date && issuedAtTime < date) {
        return true;
      } else {
        return false;
      }
    }

  }

  getRolesFromCache() {
    const cachedToken = localStorage.getItem('sessionToken');
    if (cachedToken) {
      const payload: JwtTokenPayload = this.parseJwt(JSON.parse(cachedToken));
      this.setRoles(payload.ROLES);
    }

  }

  getTokenFromCache() {
    this.setToken(JSON.parse(localStorage.getItem('sessionToken')));
  }

  setRoles(roles): void {
    this.roles = roles;
  }

  getRoles() {
    return this.roles;
  }

  setUsername(username): void {
    this.username = username;
  }

  getUsername(): string {
    return this.username;
  }


  getToken(): string {
    return this.token;
  }

  setToken(value: string) {
    this.token = value;
    this.backendService.setLoggedIn(true, value)
  }


  getAccount(): Account {
    return this.account;
  }

  setAccount(value: Account) {
    this.account = value;
  }

  isAdmin(): boolean {
    const roles: string = this.getRoles();
    if (!roles) {
      return false;
    }
    if (roles.indexOf('Admin')) {
      return true;
    } else {
      return false;
    }
  }

  createOrReceiveAddress(address: Address) {
    return this.backendService.post("/address", JSON.stringify(address));
  }
}

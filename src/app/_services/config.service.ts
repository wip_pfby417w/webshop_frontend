import { Injectable } from '@angular/core';
import {BackendService} from './backend.service';
import {Observable} from 'rxjs';
import {Config} from '../_models/config';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private backendService: BackendService) { }

  // get configuration from database
  getConfiguration(): Observable<Config[]> {
    return this.backendService.get('/config/list');
  }

  editConfig(config: Config): Observable<any> {
    const body = JSON.stringify(config);
    return this.backendService.put('/config', body);
  }

}

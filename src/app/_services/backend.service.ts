import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from "rxjs";

const baseUrl = 'http://localhost:8080';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private isLoggedIn = false;
  private token: string;

  constructor(private http: HttpClient) {
  }

  setLoggedIn(isLoggedIn: boolean, token?: string) {
    this.isLoggedIn = isLoggedIn;
    this.token = token;
  }

  buildHeader(): HttpHeaders {
    let header;
    if (this.token) {
      header = {Authorization: 'Bearer ' + this.token, 'content-type': 'application/json'};
    } else {
      header = {'content-type': 'application/json'};
    }
    return header;
  }

  request(method: string, route: string, data?: any, token?: string) {
    let header;
    if (method === 'GET') {
      return this.get(route, data);
    }

    if (token) {
      header = {Authorization: 'Bearer ' + token, 'content-type': 'application/json'};
    } else {
      header = {'content-type': 'application/json'};
    }

    return this.http.request(method, baseUrl + route, {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: header
    });
  }

  register(data?: any) {
    const header = {'content-type': 'application/json'};

    return this.http.request('POST', baseUrl + '/user/registration', {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: header
    });
  }

  get(route: string, data?: any) {
    let header = this.buildHeader();

    let params = new HttpParams();
    if (data !== undefined) {
      Object.getOwnPropertyNames(data).forEach(key => {
        params = params.set(key, data[key]);
      });
    }
    return this.http.get<any>(baseUrl + route, {
      responseType: 'json',
      headers: header,
      params
    });
  }


  login(data?: any): Observable<any> {
    const header = {'content-type': 'application/json'};

    return this.http.request('POST', baseUrl + '/signin', {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: header
    });
  }

  newDish(data?: any) {
    const header = {'content-type': 'application/json'};

    return this.http.request('POST', baseUrl + '/dish', {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: header
    });
  }

  newSize(data?: any) {
    const header = {'content-type': 'application/json'};

    return this.http.request('POST', baseUrl + '/size', {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: header
    });
  }

  post(url: String, data?: any) {
    const header = this.buildHeader();
    return this.http.request('POST', baseUrl + url, {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: header
    });
  }

  put(url: String, data: any) {
    const header = this.buildHeader();
    return this.http.request('PUT', baseUrl + url, {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: header
    });
  }


  delete(url: string, data: any) {
    const header = this.buildHeader();
    return this.http.request('DELETE', baseUrl + url, {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: header
    });
  }
}

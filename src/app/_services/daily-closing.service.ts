import { Injectable } from '@angular/core';
import {BackendService} from './backend.service';
import {Observable} from 'rxjs';
import {DailyClosing} from '../_models/daily-closing';


@Injectable({
  providedIn: 'root'
})
export class DailyClosingService {

  constructor(private backendService: BackendService) { }

  getClosing(closingDate: string, token: string): Observable<DailyClosing>{
    // TODO Error handling
    return this.backendService.get('/admin/closing/', {date: closingDate});
  }

  deleteClosing(closingDate: string): Observable<any> {
    return this.backendService.delete('/admin/closing/delete/', closingDate);
  }

}

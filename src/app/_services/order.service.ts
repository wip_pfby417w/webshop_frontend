import {Injectable} from '@angular/core';
import {BackendService} from './backend.service';
import {Order} from '../_models/order';
import {Observable} from 'rxjs';
import {OrderSizes} from '../_models/order-sizes';
import {Address} from '../_models/address';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private backendService: BackendService) { }

  getOrdersForUser(username: string, token: string): Observable<Order[]>{
    // TODO Error handling
    return this.backendService.get('/user/orders/', {account: username});
  }

  getOrderById(id: number): Observable<Order>{
    return this.backendService.get('/user/order/', {orderId: id});
  }

  getOrdersByState(token: string, state: number): Observable<Order[]>{
    return this.backendService.get('/admin/orders/', {stateId: state});
  }

  getAllOrders(token: string): Observable<Order[]>{
    return this.backendService.get('/admin/orders/');
  }

  newOrder(order: Order) {
    return this.backendService.post('/order', JSON.stringify(order));
  }

  newOrderSize(oderSize: OrderSizes) {
    return this.backendService.post('/order/size', JSON.stringify(oderSize));
  }

  setOrderState(order: Order){
    return this.backendService.post('/admin/order/state', JSON.stringify(order));
  }



}

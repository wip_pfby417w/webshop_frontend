import { TestBed } from '@angular/core/testing';

import { DailyClosingService } from './daily-closing.service';

describe('DailyClosingService', () => {
  let service: DailyClosingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DailyClosingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor() {
  }

  compressImageToLess1Mb(image) {
    return new Promise((res, rej) => {
      const oldSize = new Blob([image]).size;
      if (oldSize > 1 * 1024 * 1024) {
        this.compressImage(image, 75, 75).then(element => {
          image = element;
          res(this.compressImageToLess1Mb(image));
        });
      } else {
        res(image);
      }
    });

  }

  compressImage(src, newX, newY) {
    return new Promise((res, rej) => {
      const img = new Image();
      img.src = src;
      img.onload = () => {
        const elem = document.createElement('canvas');
        elem.width = img.width * newX / 100;
        elem.height = img.height * newX / 100;
        const ctx = elem.getContext('2d');
        ctx.drawImage(img, 0, 0, elem.width, elem.height);
        const data = ctx.canvas.toDataURL();
        res(data);
      };
      img.onerror = error => rej(error);
    });
  }


}

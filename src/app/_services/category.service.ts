import {Injectable} from '@angular/core';
import {BackendService} from './backend.service';
import {Observable} from 'rxjs';
import {Category} from '../_models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private backendService: BackendService) {
  }

  getCategories(): Observable<Category[]> {
    return this.backendService.get('/category/list');
  }

  createNew(category: Category) {
    return this.backendService.post("/category", JSON.stringify(category));
  }
}

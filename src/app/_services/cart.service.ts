import {Injectable} from '@angular/core';
import {Size} from '../_models/size';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  itemsMap: Map<Size, number> = new Map();

  constructor() {
    this.readCachedCart();
  }

  addToCart(size: Size) {
    if (this.itemsMap.has(size)) {
      const oldCount: number = this.itemsMap.get(size);
      const newCount: number = +oldCount + 1;
      this.itemsMap.set(size, newCount);
    } else {
      this.itemsMap.set(size, 1);
    }
    this.updateCachedCart();
  }

  addToCartWithAmount(size: Size, amount: number){
    if (this.itemsMap.has(size)) {
      const oldCount: number = this.itemsMap.get(size);
      const newCount: number = +oldCount + amount;
      this.itemsMap.set(size, newCount);
    }else{
      this.itemsMap.set(size, amount);
    }
    this.updateCachedCart();
  }

  getItems() {
    return this.itemsMap;
  }

  setItem(size: Size, counter: number) {
    this.itemsMap.set(size, counter);
    this.updateCachedCart();
  }

  deleteItem(item: Size) {
    this.itemsMap.delete(item);
    this.updateCachedCart();
  }

  updateCachedCart() {
    sessionStorage.removeItem('cart');
    sessionStorage.setItem('cart', JSON.stringify(Array.from(this.itemsMap)));
  }

  clearCart(){
    sessionStorage.removeItem('cart');
    this.itemsMap.clear();
  }

  readCachedCart() {
    if (sessionStorage.getItem('cart')) {
      this.itemsMap = new Map(JSON.parse(sessionStorage.getItem('cart')));
    }
  }
}

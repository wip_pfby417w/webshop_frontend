import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {BackendService} from './backend.service';
import {LoginCredential} from '../_models/login-credential';
import {AccountService} from "./account.service";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isLoggedIn = new BehaviorSubject<boolean>(false);
  private token: string;

  get getLoggedIn() {
    return this.isLoggedIn.asObservable();
  }

  setLoggedIn(isLoggedIn: boolean) {
    this.isLoggedIn.next(isLoggedIn);
  }

  constructor(private router: Router, private backendService: BackendService, private accountService: AccountService) {
    console.log('Auth Service');
    const userData = localStorage.getItem('username');
    if (userData && this.accountService.validateCachedJwt()) {
      console.log('Logged in from memory');
      const user = JSON.parse(userData);
      this.accountService.getTokenFromCache();
      this.token = this.accountService.getToken();
      this.backendService.setLoggedIn(true, this.token);
      this.accountService.setUsername(user);
      this.isLoggedIn.next(true);
    }
  }

  // user login
  login(credential: LoginCredential): Observable<any> {
    const body = JSON.stringify(credential);
    return this.backendService.login(body);
  }

  // user logout
  logout() {
    this.backendService.setLoggedIn(false, '');
    this.isLoggedIn.next(false);
    this.accountService.setUsername('');
    delete this.token;
    localStorage.clear();
    sessionStorage.clear();
    this.router.navigate(['/']);
  }
}

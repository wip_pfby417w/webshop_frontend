/**
 * @author Jonas Mertens
 * Komponente login form
 * Einloggen des Benutzers im Frontend
 */
import {Component, OnInit} from '@angular/core';
import {LoginCredential} from '../../_models/login-credential';
import {AuthService} from '../../_services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BackendService} from '../../_services/backend.service';
import {AccountService} from '../../_services/account.service';
import {JwtTokenPayload} from '../../_models/jwt-token-payload';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;
  credential: LoginCredential;
  isSubmitted = false;
  isSignInFailed = false;
  isSuccess = false;
  errorMessage = '';

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private backendService: BackendService,
    private accountService: AccountService) {
  }

  get loginFormControls() {
    return this.loginForm.controls;
  }

  /**
   * Initialisierung des Formulars
   */
  ngOnInit(): void {
    this.isSubmitted = false;
    this.isSignInFailed = false;
    this.errorMessage = '';
    this.isSuccess = false;

    this.loginForm = this.formBuilder.group(
      {
        username: ['', [Validators.required]],
        password: ['', [Validators.required]],
      },
    );
  }

  /**
   * Einloggen des Benutzers
   * Prüfung auf validen Benutzernamen und korrektes Passwort
   */
  onSubmit(): void {
    this.isSubmitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.credential = new LoginCredential();
    this.credential.username = this.loginForm.get('username').value;
    this.credential.password = this.loginForm.get('password').value;

    const response = this.authService.login(this.credential);
    response.subscribe(data => {
      this.isSubmitted = true;
      if (data.token !== undefined) {
        this.backendService.setLoggedIn(true, data.token);
        this.accountService.setToken(data.token);
        const payload: JwtTokenPayload = this.accountService.parseJwt(data.token);
        this.accountService.setRoles(payload.ROLES);
        // set local storage
        const username = this.credential.username.replace('\'', '');
        const test = username.length;
        this.accountService.setUsername(username);
        localStorage.setItem('sessionToken', JSON.stringify(data.token));
        localStorage.setItem('username', JSON.stringify(this.credential.username));
        this.isSignInFailed = false;
        this.isSuccess = true;
        this.authService.setLoggedIn(true);
        return;
      }
    }, error => {
      // TODO Error handling
      this.isSignInFailed = true;
    });

  }

  /**
   * Formular resetten
   */
  onReset(): void {
    const userStr: string = localStorage.getItem('username');

    this.isSubmitted = false;
    this.isSuccess = false;
    this.errorMessage = '';
    this.isSignInFailed = false;
    this.loginForm.reset();
  }

  /**
   * Token überprüfen
   */
  checkToken(): void {
    const userStr: string = localStorage.getItem('username');
    if (userStr.indexOf('token')) {
      this.isSuccess = true;
      this.isSubmitted = true;
    } else {
      this.isSuccess = false;
      this.isSubmitted = false;
    }
  }

}

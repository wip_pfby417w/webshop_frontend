/**
 * @author René Schiffner
 * Komponente header
 * Navigation im Kopfbereich mit Logik für Benutzernamen und Adminbereich
 */
import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../_services/auth.service';
import {AccountService} from '../../_services/account.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  loggedIn: boolean;
  username: string;

  constructor(
    private authService: AuthService,
    public accountService: AccountService
  ) {
  }

  /**
   * Initialisierung und Ermittlung ob User eingeloggt ist
   */
  ngOnInit(): void {
    this.authService.getLoggedIn.subscribe(bool => {
      console.log(bool);
      this.loggedIn = bool;
    });
    if (this.loggedIn) {
      this.username = this.accountService.getUsername();
      console.log(this.username);
    }
  }

  /**
   * Ausloggen des Benutzers
   */
  onLogout(): void {
    this.authService.logout();
    this.accountService.setRoles('');
  }

  /**
   * aktuellen Benutzernamen ermitteln
   */
  getUsername(){
    return this.username = this.accountService.getUsername();
  }
}

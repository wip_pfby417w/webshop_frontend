/**
 * @author Jonas Mertens
 * Komponente cart
 * Warenkorb im Frontend
 */
import {Component, OnInit} from '@angular/core';
import {CartService} from '../_services/cart.service';
import {DishService} from '../_services/dish.service';
import {Size} from '../_models/size';
import {Dish} from '../_models/dish';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  sizesInCartMap: Map<Size, number>;
  dishes: Dish[];

  constructor(
    private cartService: CartService,
    private dishService: DishService
  ) {
  }

  /**
   * Initialisierung
   * Holt den Warenkorb aus der Session vom Browser
   */
  ngOnInit(): void {
    this.sizesInCartMap = this.cartService.getItems();
    this.dishService.getActiveDishes().subscribe((response) => {
        console.log('resonse received');
        this.dishes = response;
      },
      error => {
        // TODO Error handling
        console.log(error);
      });
  }

  /**
   * Summe berechnen
   */
  get sum(): number {
    let sum = 0;
    for (const [key, value] of this.sizesInCartMap) {
      sum += key.price * value;
    }
    return sum;
  }

  /**
   * Speise ermitteln
   * @param size
   */
  dishForSize(size: Size): Dish {
    if (this.dishes != null) {
      const dish: Dish = this.dishes.find(element => element.dishId === size.dishId);
      return dish;
    }
  }

  /**
   * Größe und Anzahl zum Warenkorb hinzufügen
   * @param size
   * @param event
   */
  setCartItem(size: Size, event): void {
    const counter: number = event.target.value;
    this.cartService.setItem(size, counter);
  }

  /**
   * Artikel aus Warenkorb löschen
   * @param item
   */
  deleteCartItem(item: Size): void {
    this.cartService.deleteItem(item);
  }

  /**
   * Prüfen, ob Warenkorb leer ist
   */
  checkCartIsNotEmpty(){
    if (this.sizesInCartMap.size == 0){
      return false;
    }
    return true;
  }

}

/**
 * @author Jonas Mertens
 * Komponente order-items-overview
 * Artikel zu Bestellung im Frontend anzeigen
 */
import {Component, OnInit} from '@angular/core';
import {Size} from '../../_models/size';
import {Dish} from '../../_models/dish';
import {CartService} from '../../_services/cart.service';
import {DishService} from '../../_services/dish.service';
import {Config} from '../../_models/config';
import {ConfigService} from '../../_services/config.service';

@Component({
  selector: 'app-order-items-overview',
  templateUrl: './order-items-overview.component.html',
  styleUrls: ['./order-items-overview.component.css']
})
export class OrderItemsOverviewComponent implements OnInit {

  sizesInCartMap: Map<Size, number>;
  dishes: Dish[];
  configs: Config[];
  configMwst: number;

  constructor(
    private cartService: CartService,
    private dishService: DishService,
    private configService: ConfigService
  ) {
  }

  /**
   * Summe ermitteln
   */
  get sum(): number {
    let sum = 0;
    for (const [key, value] of this.sizesInCartMap) {
      sum += key.price * value;
    }
    return sum;
  }

  /**
   * Initialisierung
   * Ermittelt die Artikel aus dem Warenkorb
   */
  ngOnInit(): void {
    this.sizesInCartMap = this.cartService.getItems();
    this.dishService.getActiveDishes().subscribe((response) => {
        console.log('response received');
        this.dishes = response;
      },
      error => {
        // TODO Error handling
        console.log(error);
      });

    const configObservable = this.configService.getConfiguration();
    configObservable.subscribe((response) => {
        console.log('config received');
        this.configs = response;
      },
      error => {
        console.log(error);
      });
  }

  /**
   * Artikelgröße
   * @param size
   */
  dishForSize(size: Size): Dish {
    const dish: Dish = this.dishes.find(element => element.dishId === size.dishId);
    return dish;
  }

  /**
   * Mehrwertsteuer zur aktuellen Bestellung
   */
  getConfigMwSt(){
    if (this.configs.length > 0){
      const mwst = this.configs.filter(config => config.configName === 'mwst');
      this.configMwst = mwst[0].configValue;
    }else{
      this.configMwst = 0.19;
    }
    return this.configMwst;
  }

  /**
   * Nettopreis aus Summe rechnen
   * @param sum
   */
  getNetto(sum: number){
    const netto = sum * (-this.configMwst) + sum;
    return netto;
  }
}

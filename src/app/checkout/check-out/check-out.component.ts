/**
 * @author Jonas Mertens
 */
import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../_services/auth.service';
import {AccountService} from '../../_services/account.service';
import {first} from 'rxjs/operators';
import {Account} from '../../_models/account';
import {Address} from '../../_models/address';
import {MatDialog} from '@angular/material/dialog';
import {NewAddressComponent} from '../../new-address/new-address.component';
import {Order} from '../../_models/order';
import {OrderService} from '../../_services/order.service';
import {CartService} from '../../_services/cart.service';
import {Size} from '../../_models/size';
import {OrderSizes} from '../../_models/order-sizes';
import {ConfigService} from '../../_services/config.service';
import {Config} from '../../_models/config';
import { ChangeDetectorRef, AfterContentChecked} from '@angular/core';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})


export class CheckOutComponent implements OnInit, AfterContentChecked {

  loggedIn: boolean;
  account: Account;
  shippingAddress: Address = new Address();
  invoiceAddress: Address = new Address();
  success = false;
  paymentMethod: number;
  deliverOption: number;
  configs: Config[];
  orderMwst: number;
  firstInit = 0;

  constructor(
    private cdRef: ChangeDetectorRef,
    private authService: AuthService,
    private accountService: AccountService,
    private orderService: OrderService,
    private cartService: CartService,
    private configService: ConfigService,
    public dialog: MatDialog) {
  }

  /**
   * Initialisierung
   */
  ngOnInit(): void {
    this.authService.getLoggedIn.subscribe(bool => {
      console.log(bool);
      this.loggedIn = bool;
    });
    if (this.loggedIn) {
      this.getAccountData();
      this.getConfigMwSt();
    }
  }

  /**
   * Prüft den angezeigten Viewport auf Änderungen
   */
  ngAfterContentChecked(): void{
    this.cdRef.detectChanges();
    if (this.firstInit == 0){
      if (this.loggedIn) {
        this.getAccountData();
        this.getConfigMwSt();
        this.firstInit += 1;
      }
    }
  }

  /**
   * setzt die Standardadresse zum Account
   * @param account
   */
  setAddressFromAccount(account: Account): Address {
    const address: Address = new Address();
    address.forename = account.forename;
    address.surname = account.surname;
    address.street = account.street;
    address.housenumber = account.housenumber;
    address.postcode = account.postcode;
    address.city = account.city;
    return address;
  }

  /**
   * Adressdialog öffnen, um abweichende Adressen anzugeben
   * @param type
   */
  openAddressDialog(type): void {
    const newAddress: Address = new Address();
    const dialogRef = this.dialog.open(NewAddressComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result != false) {
        switch (type) {
          case 'shipping' :
            this.shippingAddress = result;
            break;
          case 'invoice':
            this.invoiceAddress = result;
            break;
          default:
            break;
        }
      }
    });
  }

  /**
   * neue Lieferadresse
   */
  newShippingAddress() {
    this.openAddressDialog('shipping');
  }

  /**
   * neue Rechnungsadresse
   */
  newInvoiceAddress() {
    this.openAddressDialog('invoice');
  }

  /**
   * Accountdaten aus der Datenbank holen
   */
  getAccountData() {
    const userDetailObservable = this.accountService.getUserDetails(this.accountService.getUsername(), this.accountService.getToken());

    userDetailObservable.pipe(first()).subscribe((data: Account) => {
        this.account = data;
        this.invoiceAddress = this.setAddressFromAccount(this.account);
        this.shippingAddress = this.setAddressFromAccount(this.account);
      },
      error => {
        console.log(error);
      },
      () => {
        console.log('response received');
      });
  }

  /**
   * Konfiguration der Mwst aus Backend ermitteln
   */
  getConfigMwSt(){
    const configObservable = this.configService.getConfiguration();
    configObservable.subscribe((response) => {
        this.configs = response;
      },
      error => {
        console.log(error);
      },
      () => {
        console.log('response received');
      });

    if (this.configs.length > 0){
      const mwst = this.configs.filter(config => config.configName === 'mwst');
      return mwst[0].configValue;
    }else{
      return 0.19;
    }
  }

  /**
   * Bestellung abschicken ans Backend
   */
  processOrder() {
    if (!this.validateCheckout()) {
      return;
    }
    const order = new Order();

    this.writeAddress(this.invoiceAddress).subscribe(data => {
      order.invoiceAddressId = +data;
      this.writeAddress(this.shippingAddress).subscribe(data => {
        order.shippingAddressId = +data;
        order.accountId = this.account.accountId;
        order.date = Date.now().toString();
        order.stateId = 1;
        order.paymentMethod = this.paymentMethod;
        order.deliveryOption = this.deliverOption;
        order.mwst = this.getConfigMwSt();

        // Now send order to Backend
        this.orderService.newOrder(order).subscribe(data => {
          order.orderId = +data;

          // Send selected Items for order to backend
          const sizesInCart: Map<Size, number> = this.cartService.getItems();
          let successCounter = 0;
          sizesInCart.forEach((amount, size, map) => {
            const orderSize: OrderSizes = new OrderSizes();
            orderSize.amount = amount;
            orderSize.orderId = order.orderId;
            orderSize.pricePerItem = size.price;
            orderSize.sizeId = size.sizeId;
            this.orderService.newOrderSize(orderSize).subscribe(data => {
              successCounter += 1;
              if (successCounter == sizesInCart.size) {
                this.success = true;
              }
            }, error => {
              throw new Error('Bestellter Artikel konnte nicht an Backend übertragen werden ' + error);
              this.success = false;
            });
            this.success = true;
          });
        }, error => {
          this.success = false;
          throw new Error('Bestellung konnte nicht an Backend übertragen werden ' + error);
        });

      }, error => {
        this.success = false;
        throw new Error('Unable to send shipping Address to Backend' + error);
      });

    }, error => {
      this.success = false;

      throw new Error('Unable to send invoiceAddress to Backend' + error.toString());
    });
    if (this.success == true){
      this.cartService.clearCart();
    }
  }

  /**
   * Adressen an das Backend weitergeben
   * @param address
   */
  writeAddress(address: Address) {
    return this.accountService.createOrReceiveAddress(address);
  }

  /**
   * Prüfungen bevor die Bestellung abgeschickt werden kann
   * Adresse werden geprüft / Lieferoption und Zahlungsoption geprüft
   */
  validateCheckout(): boolean {
    if (!this.validateAddress(this.shippingAddress)) {
      window.alert('Lieferadresse bitte korrigieren.');
      return false;
    }
    if (!this.validateAddress(this.invoiceAddress)) {
      window.alert('Rechnungsadrresse bitte korrigieren.');
      return false;
    }
    if (this.deliverOption == 0 || this.deliverOption == null) {
      window.alert('Bitte eine Lieferoption wählen.');
      return false;
    }
    if (this.paymentMethod == 0 || this.paymentMethod == null) {
      window.alert('Bitte eine Zahlngsart wählen.');
      return false;
    }
    return true;
  }

  /**
   * Adresse validieren
   * @param address
   */
  validateAddress(address: Address): boolean {
    if (address.forename != '' && address.surname != '' && address.street != '' && address.housenumber != ''
      && address.city != '' && address.postcode != 0) {
      return true;
    } else {
      return false;
    }
  }
}

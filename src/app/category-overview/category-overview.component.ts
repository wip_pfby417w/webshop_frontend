/**
 * @author Jonas Mertens
 * Komponente category-overview
 * Kategorien und Speisen im Frontend darstellen
 */
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CategoryService} from '../_services/category.service';
import {Category} from '../_models/category';
import {DishService} from '../_services/dish.service';
import {Dish} from '../_models/dish';
import {CartService} from '../_services/cart.service';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-category-overview',
  templateUrl: './category-overview.component.html',
  styleUrls: ['./category-overview.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CategoryOverviewComponent implements OnInit {
  categoryId: string;
  activeCategory: string[] = [];
  private sub: any;
  categories: Category[];
  dishes: Dish[];
  dishesForCategory: Dish[];
  sizeVal: any;

  constructor(
    private categoryService: CategoryService,
    private dishService: DishService,
    private cartService: CartService,
    private route: ActivatedRoute) {
  }

  /**
   * Initialisierung
   * Alle Speisen und Kategorien aus dem Backend holen
   */
  ngOnInit(): void {
    this.categoryService.getCategories().subscribe((response) => {
      console.log('response received');
      this.categories = response;
    });

    this.dishService.getActiveDishes().subscribe((response) => {
        console.log('resonse received');
        this.dishes = response;
        this.dishes.forEach(element => element.sizes = element.sizes.sort((a, b) => a.price < b.price ? -1 : 0));
      },
      error => {
        // TODO Error handling
        console.log(error);
      });
    this.getCategory();
  }

  /**
   * Aktive Kategorie aus übergebener ID holen
   * oder als Default die Kategorie Pizza setzen
   */
  getCategory(){
    // get category id from parameters
    this.sub = this.route.params.subscribe(params => {
      this.categoryId = params['cat'];
    });
    if (this.categoryId == null){
      this.categoryId = 'Pizza';
    }
    this.activeCategory = ['panel-' + this.categoryId];
  }
  /**
   * Speisen zu einer Kategorie anzeigen
   */
  getDishesForCategory(categoryId: number) {
    if (this.dishes != null){
      this.dishesForCategory = this.dishes.filter(dish => dish.categoryId === categoryId);
      if (this.dishesForCategory.length != 0){
        return this.dishesForCategory;
      }
    }
  }

  /**
   * Zum Warenkorb hinzufügen
   * @param dish
   * @param sizeId
   */
  addToCart(dish: Dish, sizeId: number) {
    const size = dish.sizes.find(element => element.sizeId == sizeId);
    this.cartService.addToCart(size);
    window.alert(dish.dish + ' Größe ' + size.size + ' wurde in den Warenkorb gelegt');
  }


}



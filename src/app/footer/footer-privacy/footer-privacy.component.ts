/**
 * @author René Schiffner
 * Komponente footer privacy
 * Keine Logik nötig - nur HTML
 */
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer-privacy',
  templateUrl: './footer-privacy.component.html',
  styleUrls: ['./footer-privacy.component.css']
})
export class FooterPrivacyComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}

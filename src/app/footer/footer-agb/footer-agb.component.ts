/**
 * @author René Schiffner
 * Komponente footer agb
 * Keine Logik nötig - nur HTML
 */
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer-agb',
  templateUrl: './footer-agb.component.html',
  styleUrls: ['./footer-agb.component.css']
})
export class FooterAgbComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FooterAgbComponent} from './footer-agb.component';

describe('FooterAgbComponent', () => {
  let component: FooterAgbComponent;
  let fixture: ComponentFixture<FooterAgbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FooterAgbComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterAgbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
